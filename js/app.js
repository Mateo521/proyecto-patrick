// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyA30TavmT8AsJx-gdfRMf8exOU1xS6EisI",
  authDomain: "administradorweb-9db42.firebaseapp.com",
  projectId: "administradorweb-9db42",
  storageBucket: "administradorweb-9db42.appspot.com",
  messagingSenderId: "1070613726487",
  appId: "1:1070613726487:web:f4fe07dd41949c2a98e352",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth();



// Función para manejar el inicio de sesión
document.addEventListener("DOMContentLoaded", function() {
    // Obtén una referencia al formulario y al botón "Submit"
    const loginForm = document.getElementById("loginForm");
    const btnSubmit = document.getElementById("btnEnviar");
  
    // Agrega un event listener al formulario para prevenir su envío predeterminado
    loginForm.addEventListener("submit", function(event) {
      event.preventDefault();
    });
  
    // Agrega un event listener al botón "Submit" para llamar a la función login() cuando se haga clic en él
    btnSubmit.addEventListener("click", login);
  
    // Función para manejar el inicio de sesión
    function login() {
      const email = document.getElementById("exampleInputEmail1").value;
      const password = document.getElementById("txtPassword").value;
  
        signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
          // Inicio de sesión exitoso
          const user = userCredential.user;
          alert("Inicio de sesión exitoso. Usuario: " + user.displayName);
          window.location.href = '/html/menu.html';
          // Puedes redirigir a otra página aquí si lo deseas
        })
        .catch((error) => {
          // Manejar errores de inicio de sesión
          const errorCode = error.code;
          const errorMessage = error.message;
          alert("Error al iniciar sesión: " + errorMessage);
        });
    }
  });
  
